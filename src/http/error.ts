import httpStatuses from 'http-status-codes';
export const httpStatus = httpStatuses;

/**
 * Web server error
 */
export interface IHttpError extends Error {
  status: number;
}

// tslint:disable-next-line:completed-docs
export class HttpError extends Error implements IHttpError {
  public readonly status: number;
  constructor(message: string, status: number) {
    super(message);
    this.status = status;
  }
}
