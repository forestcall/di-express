import { Tenants } from '../config/tenants';
import { Container } from 'inversify';
import { AcmeAccountService } from '../accounts/acme-account.service';
import {
  IAccountService,
  AccountsServiceTag
} from '../accounts/account.service';
import { LibertyAccountService } from '../accounts/liberty-account.service';
import {
  AccountsController,
  AccountsControllerTag
} from './controllers/accounts/accounts-get-all.controller';
import {
  AccountEventsControllerTag,
  AccountEventsController
} from './controllers/accounts/account-event.controller';
import {
  IAccountEventHandler,
  AccountEventHandlerTag,
  AccountEventHandlersTag
} from '../accounts/account-event-handler';
import { AccountCreatedEventHandler } from '../accounts/account-event-handler/account-created-handler.service';
import { AccountFundedEventHandler } from '../accounts/account-event-handler/account-funded-handler.service';
import { CompositeAccountEventHandler } from '../accounts/account-event-handler/composite-account-event-handler.service';

export function buildContainer(tenant: Tenants): Container {
  switch (tenant) {
    case Tenants.acme:
      return buildDefaultContainer();
    case Tenants.liberty:
      return buildLibertyContainer();
    default:
      return buildDefaultContainer();
  }
}

function buildDefaultContainer() {
  const container = new Container();
  container
    .bind<AccountsController>(AccountsControllerTag)
    .to(AccountsController);

  container.bind<IAccountService>(AccountsServiceTag).to(AcmeAccountService);

  container
    .bind<AccountEventsController>(AccountEventsControllerTag)
    .to(AccountEventsController);

  container
    .bind<IAccountEventHandler>(AccountEventHandlersTag)
    .to(AccountCreatedEventHandler);

  container
    .bind<IAccountEventHandler>(AccountEventHandlersTag)
    .to(AccountFundedEventHandler);

  container
    .bind<IAccountEventHandler>(AccountEventHandlerTag)
    .to(CompositeAccountEventHandler);

  return container;
}

function buildLibertyContainer() {
  const container = buildDefaultContainer();
  container
    .rebind<IAccountService>(AccountsServiceTag)
    .to(LibertyAccountService);

  return container;
}
