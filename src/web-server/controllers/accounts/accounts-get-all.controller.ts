import { inject, injectable } from 'inversify';
import {
  IAccountService,
  AccountsServiceTag
} from '../../../accounts/account.service';

// tslint:disable-next-line:completed-docs
@injectable()
export class AccountsController {
  private accountService: IAccountService;
  constructor(@inject(AccountsServiceTag) accountService: IAccountService) {
    this.accountService = accountService;
  }
  public getAll() {
    return this.accountService.getAll();
  }
}

export const AccountsControllerTag = Symbol(AccountsController.name);
