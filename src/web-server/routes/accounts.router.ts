import { Router } from 'express';
import {
  AccountsController,
  AccountsControllerTag
} from '../controllers/accounts/accounts-get-all.controller';
import {
  AccountEventsController,
  AccountEventsControllerTag
} from '../controllers/accounts/account-event.controller';

export const accountsRouter = Router();
accountsRouter.get('/', (req, res, next) => {
  try {
    const controller: AccountsController = req.context.container.get(
      AccountsControllerTag
    );

    console.info(controller);
    res.json(controller.getAll());
  } catch (error) {
    next(error);
  }
});

accountsRouter.post('/events', (req, res, next) => {
  try {
    const controller: AccountEventsController = req.context.container.get(
      AccountEventsControllerTag
    );

    console.info(req.body);
    console.info(controller);
    res.json(controller.handleEvent(req.body));
  } catch (error) {
    next(error);
  }
});
