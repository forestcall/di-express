import { IAccountEventHandler, IAccountEvent, accountEvents } from '.';
import { injectable } from 'inversify';

@injectable()
export class AccountCreatedEventHandler implements IAccountEventHandler {
  public handle(event: IAccountEvent) {
    if (event.eventType !== accountEvents.AccountCreated) {
      return;
    }
    console.info(`New account created: ${event.data}`);
  }
}
