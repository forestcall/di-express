import { IAccountEventHandler, IAccountEvent, accountEvents } from '.';
import { injectable } from 'inversify';

@injectable()
export class AccountFundedEventHandler implements IAccountEventHandler {
  public handle(event: IAccountEvent) {
    if (event.eventType !== accountEvents.AccountFunded) {
      return;
    }
    console.info(`Account was funded: ${event.data}`);
  }
}
