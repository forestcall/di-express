export interface IAccountEventHandler {
  handle(event: IAccountEvent): any;
}

export enum accountEvents {
  AccountCreated = 'AccountCreated',
  AccountFunded = 'AccountFunded'
}

export interface IAccountEvent {
  eventType: accountEvents;
  data: any;
}

export const AccountEventHandlerTag = Symbol('AccountEventHandler');

export const AccountEventHandlersTag = Symbol('AccountEventHandlers');
