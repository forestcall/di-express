import { IAccount } from './account.model';

export interface IAccountService {
  getAll(): IAccount[];
}

export const AccountsServiceTag = Symbol('AccountsService');
