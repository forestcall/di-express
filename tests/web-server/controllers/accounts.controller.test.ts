import { AccountsController } from '../../../src/web-server/controllers/accounts/accounts-get-all.controller';
import { AcmeAccountService } from '../../../src/accounts/acme-account.service';
import { expect } from 'chai';
import { Account } from '../../../src/accounts/account.model';

describe('AccountsController', () => {
  describe('#getAll', () => {
    it('returns all accounts', () => {
      const controller = new AccountsController(new AcmeAccountService());
      expect(controller.getAll()).to.deep.eq([new Account(1, 2000, 'acme')]);
    });
  });
});
