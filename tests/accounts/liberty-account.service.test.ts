import { expect } from 'chai';
import { Account } from '../../src/accounts/account.model';
import { LibertyAccountService } from '../../src/accounts/liberty-account.service';

describe('LibertyAccountService', () => {
  describe('#getAll', () => {
    it('returns an array of Accounts', () => {
      const service = new LibertyAccountService();
      const result = service.getAll();
      expect(result).to.deep.eq([new Account(100, 99999, 'liberty')]);
    });
  });
});
