import { AcmeAccountService } from '../../src/accounts/acme-account.service';
import { expect } from 'chai';
import { Account } from '../../src/accounts/account.model';

describe('AcmeAccountService', () => {
  describe('#getAll', () => {
    it('returns an array of Accounts', () => {
      const service = new AcmeAccountService();
      const result = service.getAll();
      expect(result).to.deep.eq([new Account(1, 2000, 'acme')]);
    });
  });
});
